'use strict';

jQuery(function($) {

// Inits


  // vars screens

  $('#serviceSelect2').focus(function() {
    $(this).parent().parent().fadeTo('slow', 1);
  });

  var screens;
  window.onresize = function() {

    $('#new-schedule').fadeIn();
    if (($(window).width() < 768) && (screens !== 'xs')) {
      screens = 'xs';
      console.log(screens);

      $('#tolls').hide('slide');
      $('#aside').hide('slide');
      $('#new').hide('slide');

      $('#tolls').delay(350).animate({
        width: '0%'
      });
      $('#aside').delay(350).animate({
        width: '0%'
      });
      $('#index').delay().animate({
        width: '100%'
      });
    } else if (($(window).width() >= 768) && ($(window).width() < 992) && (screens !== 'sm')) {
      screens = 'sm';
      console.log(screens);

      $('#tolls').show('slide');
      $('#aside').hide('slide');
      $('#new').hide('slide');

      $('#tolls').delay(350).animate({
        width: '20%'
      });
      $('#aside').delay(350).animate({
        width: '0%'
      });
      $('#index').delay(0).animate({
        width: '80%'
      });

    } else if (($(window).width() >= 992) && ($(window).width() < 1200) && (screens !== 'md')) {
      screens = 'md';
      console.log(screens);

      $('#tolls').show('slide');
      $('#aside').show('slide');

      $('#tolls').delay(350).animate({
        width: '10%'
      });
      $('#aside').delay(350).animate({
        width: '25%'
      });
      $('#index').delay(0).animate({
        width: '65%'
      });
      $('#new').delay(350).animate({
        width: '30%'
      });

    } else if (($(window).width() >= 1200) && ($(window).width() < 1400) && (screens !== 'lg')) {
      screens = 'lg';
      console.log(screens);

      $('#tolls').show('slide');
      $('#aside').show('slide');

      $('#tolls').delay(350).animate({
        width: '9%'
      });
      $('#aside').delay(350).animate({
        width: '21%'
      });
      $('#index').delay(550).animate({
        width: '69%'//.70
      });
      $('#new').delay(350).animate({
        width: '30%'
      });

    } else if (($(window).width() >= 1400) && (screens !== 'xlg')) {
      screens = 'xlg';
      console.log(screens);

      $('#tolls').show('slide');
      $('#aside').show('slide');
      $('#tolls').delay(350).animate({
        width: '7%'
      });
      $('#aside').delay(350).animate({
        width: '18%'
      });
      $('#index').delay(350).animate({
        width: '75%'//.75
      });
      $('#new').delay(350).animate({
        width: '20%'
      });
    }
  };



  window.onresize();



  var inputClass = 1;
  $('input').keydown(function(event) {
    // event.preventDefault();
    if (event.keyCode === 13) {
      console.log('key ok')
      inputClass++;
      setTimeout(function() {
      $('.f' + inputClass).delay(200).focus();
      console.log('enter pressionado');
    },500);
    }
  });

  $('#new-schedule').click(function() {
    $(this).fadeOut('fast');

    $('#clientSelect').addClass('f1').focus(function() {
      inputClass = 1;
    });;
    $('#datePicker').addClass('f2').focus(function() {
      inputClass = 2;
    });;;
    $('#timePicker').addClass('f3').focus(function() {
      inputClass = 3;
    });;;
    $('#psTextArea').addClass('f4').focus(function() {
      inputClass = 4;
    });;;
    $('#serviceSelect1').addClass('f5').focus(function() {
      inputClass = 5;
    });;;
    $('#employeeSelect1').addClass('f6').focus(function() {
      inputClass = 6;
    });;;
    // $('#serviceSelect2').addClass('f7');
    // $('#employeeSelect2').addClass('f8');
    // $('#serviceSelect3').addClass('f9');
    // $('#employeeSelect3').addClass('f10');
    // $('#serviceSelect4').addClass('f11');
    // $('#employeeSelect4').addClass('f12');
    // $('#serviceSelect5').addClass('f13');
    // $('#employeeSelect5').addClass('f14');
    if (screens === 'md') {
      $('#index').delay(350).animate({
        width: '55%'
      });
      $('#aside').delay(350).hide('slide');
      $('#new').delay(550).animate({
        width: '35%'
      });
      $('#new').delay(350).fadeIn();


    } else if (screens === 'lg') {
      $('#index').delay(350).animate({
        width: '61%'
      });
      $('#aside').delay(350).hide('slide');
      $('#new').delay(550).animate({
        width: '30%'
      });
      $('#new').delay(350).fadeIn();
      // $('#new').delay(550).show();

    } else if (screens === 'xlg') {
      $('#aside').delay(350).hide('slide');
      $('#index').delay(350).animate({
        width: '68%'
      });
      $('#new').delay(550).animate({
        width: '25%'
      });
      $('#new').delay(350).fadeIn();
    }

    setTimeout(function() {
      $('#clientSelect').focus();
    }, 1400);
  });

  $('#cancelNewSche').click(function() {
    $('#new-schedule').fadeIn('fast');

    if (screens === 'lg') {
      $('#tolls').show('slide');
      $('#aside').show('slide');

      $('#tolls').delay(350).animate({
        width: '9%'
      });
      $('#aside').delay(350).animate({
        width: '21%'
      });
      $('#index').delay(550).animate({
        width: '70%'
      });
      $('#new').delay(350).animate({
        width: '30%'
      });
      $('#new').delay(250).fadeOut();

      // $('#new').delay(550).show();




    } else if (screens === 'md') {
      $('#tolls').show('slide');
      $('#aside').show('slide');

      // $('#tolls').delay(350).animate({
      //   width: '7%'
      // });
      $('#aside').delay(350).animate({
        width: '25%'
      });
      $('#index').delay(0).animate({
        width: '65%'
      });
      $('#new').delay(350).animate({
        width: '30%'
      });
      $('#new').delay(250).fadeOut();



    } else if (screens === 'xlg') {
      $('#tolls').show('slide');
      $('#aside').show('slide');

      $('#tolls').delay(350).animate({
        width: '7%'
      });
      $('#aside').delay(350).animate({
        width: '18%'
      });
      $('#index').delay(350).animate({
        width: '75%'
      });
      $('#new').delay(350).animate({
        width: '20%'
      });
      $('#new').delay(250).fadeOut();
    }
  });


$('#serviceSelect2 ,#serviceSelect3 ,#serviceSelect4 ,#serviceSelect5 ').focus(function() {
    $(this).parent().css('opacity', '1');
    console.log(" foxus")
});
var colVisible = 1;
var scrollToEl = $(".trr");
$('.seta-baixo').click(function() {
  console.log(colVisible)
  scrollToEl[1].scrollIntoView();
  colVisible++;
});




  $('input , textarea').focus(function() {
    $('.input-help').fadeOut();
    $(this).parent().find('.input-help').fadeIn();
  });

  $('#index').fadeIn(1500);

  $('#employeeSelect , #employeeSelect2, #employeeSelect3, #employeeSelect4, #employeeSelect5 ').click(function() {
    // body...
  });

  $('#serviceSelect, #serviceSelect2, #serviceSelect3, #serviceSelect4, #serviceSelect5').click(function() {
    // body...
  });





});
