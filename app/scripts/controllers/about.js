'use strict';

/**
 * @ngdoc function
 * @name lanzoApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the lanzoApp
 */
angular.module('lanzoApp')
  .controller('AboutCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
