'use strict';

angular.module('lanzoApp')
  .controller('SystemCtrl', function($scope) {

    $scope.inputNumber = 1;
    $scope.addInput = function() {
      $('newInput' + $scope.inputNumber).fadeTo('slow', 1);
      $('');
      $scope.inputs = $scope.inputs + 1;
      $('.newInput' + $scope.inputs).show();
      console.log($scope.input);
    };

    setTimeout(function() {      
      console.log('123');
      // $('#calaside').fadeIn()
      $('#aside').fadeTo('fast', 1);
    });

    //DatePicker
    $scope.today = function() {
      $scope.dt = new Date();
    };
    $scope.today();

    $scope.clear = function() {
      $scope.dt = null;
    };

    // Disable weekend selection
    $scope.disabled = function(date, mode) {
      return (mode === 'day' && (date.getDay() === 0 || date.getDay() === 6));
    };

    $scope.toggleMin = function() {
      $scope.minDate = $scope.minDate ? null : new Date();
    };
    $scope.toggleMin();

    $scope.open = function($event) {
      $event.preventDefault();
      $event.stopPropagation();


      $scope.opened = true;
    };

    $scope.dateOptions = {
      formatYear: 'yy',
      startingDay: 1
    };



    $scope.clients = [
      {
        name: 'Marta',
        id: 1,
        'cel': '9831 1323'
      }, {
        name: 'Adelha',
        id: 2,
        'cel': '9831 1323'
      }, {
        name: 'Dilma',
        id: 3,
        'cel': '9831 1323'
      }, {
        name: 'Mariana',
        id: 4,
        'cel': '9831 1323'
      }, {
        name: 'Marcela',
        id: 5,
        'cel': '9831 1323'
      }, {
        name: 'Jaci',
        id: 7,
        'cel': '9831 1323'
      }, {
        name: 'Valentina',
        id: 6,
        'cel': '9831 1323}'
      }
    ];

    $scope.services = [

        {
          name: 'progressiva',
          id: 1,
          employees:[{name: 'Marta'}, {name: 'Marcia'}]
        },
        {
          name: 'unha',
          id: 2,
          employees: [{name: 'Adelha'}, {name: 'Marcia'}]
        },
        {
          name: 'escova',
          id: 3,
          employees: [{name: 'Dilma'}, {name: 'Marcia'}]
        },
        {
          name: 'hidratação',
          id: 4,
          employees: [{name: 'Mariana'}, {name: 'Marcia'}]
        },
        {
          name: 'depilação',
          id: 5,
          employees: [{name: 'Marcela'}, {name: 'Marcia'}]
        },
        {
          name: 'corte',
          id: 4,
          employees: [{name: 'Jaci'}, {name: 'Marcia'}]
        },
        {
          name: 'maquiagem',
          id: 6,
          employees: [{name: 'Valentina'}, {name: 'Marcia'}]
        }
    ];

    $scope.schedules = [
      {
        hour: '12:20',
        client: 'Adelha',
        employee: 'Cida',
        service: ['unha', 'pé']
      },
      {
        hour: '12:20',
        client: 'Marcia',
        employee: 'Eide',
        service: ['corte', 'hidratação']
      },
      {
        hour: '12:20',
        client: 'Dilma',
        employee: 'Vivian',
        service: ['hidratação', 'depilação']
      },
      {
        hour: '12:20',
        client: 'Joana',
        employee: 'Jhey',
        service: ['corte', 'tintura']
      },
      {
        hour: '12:20',
        client: 'Marcela',
        employee: 'Zelia',
        service: ['unha', 'relaxamento']
      },
      {
        hour: '12:20',
        client: 'Adelha',
        employee: 'Jaci',
        service: ['unha']
      },
      {
        hour: '12:20',
        client: 'Julia',
        employee: 'Valentina',
        service: ['unha', 'pé']
      },
      {
        hour: '12:20',
        client: 'Estefanie',
        employee: 'João Plenario',
        service: ['unha', 'sobrancelha']
      },
      {
        hour: '12:20',
        client: 'Carla',
        employee: 'Errick Pereira',
        service: ['unha', 'corte']
      },
      {
        hour: '12:20',
        client: 'Isabella',
        employee: 'Valentina K',
        service: ['unha']
      },
      {
        hour: '12:20',
        client: 'Renata',
        employee: 'Longa Vida',
        service: ['unha' ]
      },
      {
        hour: '12:20',
        client: 'Tarissa',
        employee: 'Jose Antides',
        service: ['corte']
      },
      {
        hour: '12:20',
        client: 'Aline',
        employee: 'Macaco',
        service: ['unha', 'alisamento']
      },
      {
        hour: '12:20',
        client: 'Fabio',
        employee: 'Crispim de Brito',
        service: ['unha', 'progressiva']
      },
      {
        hour: '12:20',
        client: 'Francisca',
        employee: 'Vanderlan ',
        service: ['unha', 'massoterapia']
      },
      {
        hour: '12:20',
        client: 'Diva',
        employee: 'Cida',
        service: ['unha']
      }
    ];

});





