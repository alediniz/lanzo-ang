'use strict';

/**
 * @ngdoc function
 * @name lanzoApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the lanzoApp
 */
angular.module('lanzoApp')
  .controller('MainCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
