'use strict';

/**
 * @ngdoc overview
 * @name lanzoApp
 * @description
 * # lanzoApp
 *
 * Main module of the application.
 */
angular
  .module('lanzoApp', [
    'ngAnimate',
    'ngAria',
    'ngCookies',
    'ngMessages',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'ui.bootstrap',
    'ui.utils'
    // 'ngClock'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/system.html',
        controller: 'SystemCtrl'

      })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  })

